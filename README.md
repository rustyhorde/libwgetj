# libwgetj
Library wrapping wget to grab Java distributions.

## Version
[![Crates.io](https://img.shields.io/crates/v/libwgetj.svg)](https://crates.io/crates/libwgetj)
[![Build Status](https://travis-ci.org/rustyhorde/libwgetj.svg?branch=master)](https://travis-ci.org/rustyhorde/libwgetj)
