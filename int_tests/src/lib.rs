#![allow(unstable)]
extern crate libwgetj;

#[cfg(test)]
mod test {
    use std::os;
    use std::io::fs;
    use std::io::fs::PathExtensions;
    use libwgetj::DownloadConfig;

    #[test]
    fn test_download() {
        let tmp = os::tmpdir().join("jdk-8u25-linux-x64.tar.gz");
        let mut cfg = DownloadConfig::new();
        // actual download
        cfg.dry_run(false);
        assert_eq!(Ok(0), cfg.download());
        assert!(tmp.exists());
        assert!(fs::unlink(&tmp).is_ok());
    }
}
