//! libwgetj API errors
use std::error::Error;
use std::fmt;

#[derive(Debug, PartialEq)]
/// An error thrown by the libwgetj library.
pub struct LibwgetjError {
    /// The error description.
    desc: String,
    /// The error detail.
    detail: String,
}

impl LibwgetjError {
    /// Create a new LibwgetjError with the given description and detail.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::LibwgetjError;
    ///
    /// let err = LibwgetjError::new("Download Failure", "Unable to connect to server!");
    /// ```
    pub fn new<T: fmt::Display>(desc: &str, detail: T) -> LibwgetjError
        where T: fmt::Debug
    {
        LibwgetjError {
            desc: desc.to_owned(),
            detail: format!("{}", detail),
        }
    }
}

impl fmt::Display for LibwgetjError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{}: {}", self.desc, self.detail)
    }
}

impl Error for LibwgetjError {
    fn description(&self) -> &str {
        &self.desc
    }
}

impl From<::std::io::Error> for LibwgetjError {
    fn from(e: ::std::io::Error) -> LibwgetjError {
        LibwgetjError::new("IO Error", e)
    }
}
