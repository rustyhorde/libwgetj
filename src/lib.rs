//! **libwgetj API**
//!
//! Wraps wget with some cookie management to allow downloading Java releases from the command line.
//!
//! # Examples
//!
//! ```
//! use libwgetj::{DownloadConfig, latest};
//! use libwgetj::Arch::*;
//! use libwgetj::Archive::*;
//! use libwgetj::OS::*;
//! use libwgetj::Package::*;
//! use libwgetj::Version::*;
//!
//! // Setup a dry run (don't actually download) for the default Java 8 JDK 64-bit Linux tar.gz
//! // (latest point release).
//! let mut cfg: DownloadConfig = Default::default();
//! cfg.dry_run(true);
//! match cfg.download() {
//!     Ok(res) => assert!(res == 0),
//!     Err(e)  => { println!("{:?}", e); assert!(false) },
//! }
//!
//! // Download Java 8 JDK 64-bit Linux tar.gz (older point release).
//! let mut cfg: DownloadConfig = Default::default();
//! cfg.point_release(40).dry_run(true);
//! match cfg.download() {
//!     Ok(res) => assert!(res == 0),
//!     Err(e)  => { println!("{:?}", e); assert!(false) },
//! }
//!
//! // Download Java 8 JDK 64-bit Mac OSX dmg (latest point release).
//! let mut cfg: DownloadConfig = Default::default();
//! cfg.archive(DMG).os(Mac).dry_run(true);
//! match cfg.download() {
//!     Ok(res) => assert!(res == 0),
//!     Err(e)  => { println!("{:?}", e); assert!(false) },
//! }
//!
//! // Download Java 8 JDK 32-bit Windows exe (latest point release).
//! let mut cfg: DownloadConfig = Default::default();
//! cfg.archive(EXE).os(Windows).arch(I586).dry_run(true);
//! match cfg.download() {
//!     Ok(res) => assert!(res == 0),
//!     Err(e)  => { println!("{:?}", e); assert!(false) },
//! }
//!
//! // Download Java 8 JRE 32-bit Linux rpm (latest point release).
//! let mut cfg: DownloadConfig = Default::default();
//! cfg.archive(RPM).arch(I586).package(JRE).dry_run(true);
//! match cfg.download() {
//!     Ok(res) => assert!(res == 0),
//!     Err(e)  => { println!("{:?}", e); assert!(false) },
//! }
//!
//! // Download Java 7 JDK 64-bit Linux tar.gz (latest point release).
//! let mut cfg: DownloadConfig = Default::default();
//! cfg.version(Seven).point_release(latest(Seven)).dry_run(true);
//! match cfg.download() {
//!     Ok(res) => assert!(res == 0),
//!     Err(e)  => { println!("{:?}", e); assert!(false) },
//! }
//! ```
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy, clippy_pedantic))]
#![deny(missing_docs)]
extern crate commandext;
#[macro_use]
extern crate log;

use commandext::CommandExt;
use cookies::*;
use std::borrow::Borrow;
use std::env;
use std::path::PathBuf;
use std::process::{Command, Stdio};

// Re-exports
pub use errors::LibwgetjError;
pub use types::Arch;
pub use types::Arch::*;
pub use types::Archive;
pub use types::Archive::*;
pub use types::OS;
pub use types::OS::*;
pub use types::Package;
pub use types::Package::*;
pub use types::Version;
pub use types::Version::*;
pub use version::version;

mod cookies;
mod errors;
mod types;
mod version;

/// Base Download URL
static BASEURL: &'static str = "http://download.oracle.com/otn-pub/java/jdk/";

/// The Java download configuration. The download function will read from this configuration and
/// build a url specific to the configuration.
///
/// # Defaults
///
/// * Package: JDK
/// * Version: 8
/// * Point Release: Latest for 8
/// * OS: Linux
/// * CPU Arch: 64-bit
/// * Archive Type: .tar.gz
/// * Download Directory: None
/// * Dry Run: false
///
pub struct DownloadConfig {
    /// Package (JDK or JRE)
    pkg: Package,
    /// Version (Seven or Eight)
    ver: Version,
    /// Point Release
    pr: u8,
    /// OS Type (Linux, Mac OS, or Windows)
    os: OS,
    /// OS Architecture (32 or 64)
    arch: Arch,
    /// Archive Type (tar.gz, or .exe, or .dmg)
    am: Archive,
    /// Directory to save to
    dir: Option<PathBuf>,
    /// Dry Run (don't actually download)
    dr: bool,
}

impl Default for DownloadConfig {
    /// Create a new download configuration with some defaults.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// ```
    fn default() -> DownloadConfig {
        DownloadConfig {
            pkg: JDK,
            ver: Eight,
            pr: latest(Eight),
            os: Linux,
            arch: X64,
            am: TGZ,
            dir: None,
            dr: false,
        }
    }
}

impl DownloadConfig {
    /// Set the package type.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    /// use libwgetj::Package::*;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// cfg.package(JRE);
    /// ```
    pub fn package(&mut self, pkg: Package) -> &mut DownloadConfig {
        self.pkg = pkg;
        debug!("Package Set: {}", pkg);
        self
    }

    /// Set the version.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    /// use libwgetj::Version::*;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// cfg.version(Seven);
    /// ```
    pub fn version(&mut self, ver: Version) -> &mut DownloadConfig {
        self.ver = ver;
        debug!("Version Set: {}", ver);
        self
    }

    /// Set the point release information.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::{DownloadConfig, latest};
    /// use libwgetj::Version::*;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// cfg.version(Seven).point_release(latest(Seven));
    /// ```
    pub fn point_release(&mut self, pr: u8) -> &mut DownloadConfig {
        self.pr = pr;
        self
    }

    /// Set the OS type.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    /// use libwgetj::OS::*;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// cfg.os(Windows);
    /// ```
    pub fn os(&mut self, os: OS) -> &mut DownloadConfig {
        self.os = os;
        self
    }

    /// Set the architecture (32 or 64-bit).
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    /// use libwgetj::Arch::*;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// cfg.arch(I586);
    /// ```
    pub fn arch(&mut self, arch: Arch) -> &mut DownloadConfig {
        self.arch = arch;
        self
    }

    /// Set the archive type.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    /// use libwgetj::Archive::*;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// cfg.archive(EXE);
    /// ```
    pub fn archive(&mut self, archive: Archive) -> &mut DownloadConfig {
        self.am = archive;
        self
    }

    /// Set the target directory for the download.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    /// use std::env;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// cfg.dir(Some(env::temp_dir()));
    /// ```
    pub fn dir(&mut self, path: Option<PathBuf>) -> &mut DownloadConfig {
        self.dir = path;
        self
    }

    /// Set the dry run status.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// cfg.dry_run(true);
    /// ```
    pub fn dry_run(&mut self, enable: bool) -> &mut DownloadConfig {
        self.dr = enable;
        self
    }

    /// Check the configuration for invalid combinations.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    /// use libwgetj::Archive::*;
    /// use libwgetj::OS::*;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// // Invalid combination, Mac only has DMG downloads.
    /// cfg.os(Mac).archive(TGZ).dry_run(true);
    /// assert!(cfg.check_config().is_err());
    /// ```
    pub fn check_config(&self) -> Result<(), LibwgetjError> {
        let mut errors = Vec::new();
        let os = self.os;

        match self.arch {
            I586 if os == Mac => {
                errors.push("Java only has 64-bit distributions for Mac");
            }
            _ => {}
        }

        match self.am {
            TGZ if os == Mac || os == Windows => {
                errors.push("Mac/Windows distributions not available in .tar.gz format");
            }
            RPM if os == Mac || os == Windows => {
                errors.push("Mac/Windows distributions not available in .rpm format");
            }
            DMG if os == Linux || os == Windows => {
                errors.push("Linux/Windows distributions not available in .dmg format");
            }
            EXE if os == Linux || os == Mac => {
                errors.push("Linux/Mac distributions not available in .exe format");
            }
            _ => {}
        }

        let pr = self.ver.point_releases();

        if !pr.contains_key(&self.pr) {
            errors.push("Invalid point release for the given major version");
        }

        if errors.is_empty() {
            Ok(())
        } else {
            let mut error_str = String::new();
            for error in &errors {
                error_str.push_str(error);
                error_str.push('\n');
            }

            Err(LibwgetjError::new("Invalid Configuration", error_str))
        }
    }

    /// Generate a command result.
    fn gen_result(&self) -> fn(cmd: &mut Command) -> Result<i32, LibwgetjError> {
        /// Inner Result Function
        fn resfn(cmd: &mut Command) -> Result<i32, LibwgetjError> {
            cmd.stdout(Stdio::inherit());
            cmd.stderr(Stdio::inherit());

            match cmd.spawn() {
                Ok(mut p) => {
                    match p.wait() {
                        Ok(status) => {
                            match status.code() {
                                Some(code) => {
                                    if code == 0 {
                                        Ok(code)
                                    } else {
                                        Err(LibwgetjError::new("Command Failed", code))
                                    }
                                }
                                None => Err(LibwgetjError::new("Command Failed", "Unknown code!")),
                            }
                        }
                        Err(e) => Err(LibwgetjError::new("Command Failed", e)),
                    }
                }
                Err(e) => Err(LibwgetjError::new("Command Failed", e)),
            }
        };
        resfn
    }

    /// Execute the download.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::DownloadConfig;
    ///
    /// let mut cfg: DownloadConfig = Default::default();
    /// assert!(cfg.dry_run(true).download().is_ok());
    /// ```
    pub fn download(&self) -> Result<i32, LibwgetjError> {
        let url = try!(build_url(self));

        if self.dr {
            info!("Not downloading: {}", url);
            Ok(0)
        } else {
            delete_tmp_files().and(write_tmp_files(self).and_then(|p| {
                let tmp = env::temp_dir();
                let d = match self.dir {
                    Some(ref d) => d,
                    None => &tmp,
                };
                CommandExt::new("wget")
                    .arg("-c")
                    .arg("--load-cookies")
                    .arg(p.to_string_lossy().borrow())
                    .arg("-P")
                    .arg(d.to_string_lossy().borrow())
                    .arg(&url[..])
                    .header(true)
                    .exec(self.gen_result())
                    .and(delete_tmp_files())
            }))
        }
    }
}

/// Produce the latest point release version for each supported major version.
///
/// # Examples
///
/// ```
/// use libwgetj::{DownloadConfig, latest};
/// use libwgetj::Version::*;
///
/// let mut cfg: DownloadConfig = Default::default();
/// cfg.version(Seven).point_release(latest(Seven));
/// ```
pub fn latest(ver: Version) -> u8 {
    if let Some(pr) = ver.point_releases().keys().max() {
        *pr
    } else {
        error!("Unable to determine latest point release!");
        0
    }
}

/// Generate the download URL version.
fn vs(cfg: &DownloadConfig) -> String {
    if cfg.pr == 0 {
        format!("{}", cfg.ver)
    } else {
        format!("{}u{}", cfg.ver, cfg.pr)
    }
}

/// Generate the download URL suffix
fn suffix(cfg: &DownloadConfig) -> String {
    let pr = cfg.ver.point_releases();
    let mut res = String::new();
    if let Some(s) = pr.get(&cfg.pr) {
        res = format!("-b{:02}", s);

        if cfg.ver == Eight {
            match cfg.pr {
                121 => res.push_str("/e9e7ea248e2c4826b92b3f075a80e441"),
                131 => res.push_str("/d54c1d3a095b4ff2b6607d096fa80163"),
                _ => {}
            }
        }
    }
    res
}

/// Build the download URL from the component parts
fn build_url(cfg: &DownloadConfig) -> Result<String, LibwgetjError> {
    try!(cfg.check_config());
    let vs = vs(cfg);
    Ok(format!("{}{}{}/{}-{}-{}-{}.{}",
               BASEURL,
               vs,
               suffix(cfg),
               cfg.pkg,
               vs,
               cfg.os,
               cfg.arch,
               cfg.am))
}

#[cfg(test)]
#[cfg_attr(feature = "clippy", allow(stutter))]
mod test {
    use super::*;
    use super::build_url;

    static A: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jdk-8u131-linux-x64.tar.gz";
    static B: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jdk-8u131-linux-i586.tar.gz";
    static C: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jdk-8u131-linux-x64.rpm";
    static D: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jdk-8u131-linux-i586.rpm";
    static E: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jdk-8u131-macosx-x64.dmg";
    static F: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jdk-8u131-windows-x64.exe";
    static G: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jdk-8u131-windows-i586.exe";
    static H: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jre-8u131-linux-x64.tar.gz";
    static I: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jre-8u131-linux-i586.tar.gz";
    static J: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jre-8u131-linux-x64.rpm";
    static K: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jre-8u131-linux-i586.rpm";
    static L: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jre-8u131-macosx-x64.dmg";
    static M: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jre-8u131-windows-x64.exe";
    static N: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/\
                              jre-8u131-windows-i586.exe";

    static O: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jdk-7u80-linux-x64.tar.gz";
    static P: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jdk-7u80-linux-i586.tar.gz";
    static Q: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jdk-7u80-linux-x64.rpm";
    static R: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jdk-7u80-linux-i586.rpm";
    static S: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jdk-7u80-macosx-x64.dmg";
    static T: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jdk-7u80-windows-x64.exe";
    static U: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jdk-7u80-windows-i586.exe";
    static V: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jre-7u80-linux-x64.tar.gz";
    static W: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jre-7u80-linux-i586.tar.gz";
    static X: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jre-7u80-linux-x64.rpm";
    static Y: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jre-7u80-linux-i586.rpm";
    static Z: &'static str = "http://download.oracle.\
                              com/otn-pub/java/jdk/7u80-b15/jre-7u80-macosx-x64.dmg";
    static AA: &'static str = "http://download.oracle.\
                               com/otn-pub/java/jdk/7u80-b15/jre-7u80-windows-x64.exe";
    static AB: &'static str = "http://download.oracle.\
                               com/otn-pub/java/jdk/7u80-b15/jre-7u80-windows-i586.exe";

    #[test]
    fn url_suffix_7() {
        let pr = Seven.point_releases();

        // Java 7 mappings
        assert_eq!(Some(&0), pr.get(&0));
        assert_eq!(Some(&8), pr.get(&1));
        assert_eq!(Some(&13), pr.get(&2));
        assert_eq!(Some(&4), pr.get(&3));
        assert_eq!(Some(&20), pr.get(&4));
        assert_eq!(Some(&6), pr.get(&5));
        assert_eq!(Some(&24), pr.get(&6));
        assert_eq!(Some(&10), pr.get(&7));
        assert_eq!(Some(&5), pr.get(&9));
        assert_eq!(Some(&18), pr.get(&10));
        assert_eq!(Some(&21), pr.get(&11));
        assert_eq!(Some(&20), pr.get(&13));
        assert_eq!(Some(&3), pr.get(&15));
        assert_eq!(Some(&2), pr.get(&17));
        assert_eq!(Some(&11), pr.get(&21));
    }

    #[test]
    fn url_suffix_7_1() {
        let pr = Seven.point_releases();

        assert_eq!(Some(&15), pr.get(&25));
        assert_eq!(Some(&43), pr.get(&40));
        assert_eq!(Some(&18), pr.get(&45));
        assert_eq!(Some(&13), pr.get(&51));
        assert_eq!(Some(&13), pr.get(&55));
        assert_eq!(Some(&19), pr.get(&60));
        assert_eq!(Some(&17), pr.get(&65));
        assert_eq!(Some(&1), pr.get(&67));
        assert_eq!(Some(&14), pr.get(&71));
        assert_eq!(Some(&14), pr.get(&72));
        assert_eq!(Some(&13), pr.get(&75));
        assert_eq!(Some(&13), pr.get(&76));
        assert_eq!(Some(&15), pr.get(&79));
        assert_eq!(Some(&15), pr.get(&80));
    }

    #[test]
    fn url_suffix_8() {
        let pr = Eight.point_releases();

        // Java 8 mappings
        assert_eq!(Some(&132), pr.get(&0));
        assert_eq!(Some(&13), pr.get(&5));
        assert_eq!(Some(&12), pr.get(&11));
        assert_eq!(Some(&26), pr.get(&20));
        assert_eq!(Some(&17), pr.get(&25));
        assert_eq!(Some(&13), pr.get(&31));
        assert_eq!(Some(&26), pr.get(&40));
        assert_eq!(Some(&14), pr.get(&45));
        assert_eq!(Some(&16), pr.get(&51));
        assert_eq!(Some(&27), pr.get(&60));
        assert_eq!(Some(&17), pr.get(&65));
        assert_eq!(Some(&17), pr.get(&66));
        assert_eq!(Some(&15), pr.get(&71));
        assert_eq!(Some(&2), pr.get(&74));
        assert_eq!(Some(&3), pr.get(&77));
        assert_eq!(Some(&14), pr.get(&91));
        assert_eq!(Some(&14), pr.get(&92));
        assert_eq!(Some(&13), pr.get(&101));
        assert_eq!(Some(&14), pr.get(&102));
        assert_eq!(Some(&15), pr.get(&112));
    }

    #[test]
    fn jdk8_build_url() {
        let mut cfg: DownloadConfig = Default::default();

        // 8u JDKs
        assert_eq!(Ok(A.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(B.to_owned()), build_url(&cfg));
        cfg.arch(Arch::X64);
        cfg.archive(Archive::RPM);
        assert_eq!(Ok(C.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(D.to_owned()), build_url(&cfg));
        cfg.arch(Arch::X64);
        cfg.os(OS::Mac);
        cfg.archive(Archive::DMG);
        assert_eq!(Ok(E.to_owned()), build_url(&cfg));
        cfg.os(OS::Windows);
        cfg.archive(Archive::EXE);
        assert_eq!(Ok(F.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(G.to_owned()), build_url(&cfg));
    }

    #[test]
    fn jre8_build_url() {
        let mut cfg: DownloadConfig = Default::default();

        // 8u JREs
        cfg.package(Package::JRE);
        assert_eq!(Ok(H.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(I.to_owned()), build_url(&cfg));
        cfg.arch(Arch::X64);
        cfg.archive(Archive::RPM);
        assert_eq!(Ok(J.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(K.to_owned()), build_url(&cfg));
        cfg.arch(Arch::X64);
        cfg.os(OS::Mac);
        cfg.archive(Archive::DMG);
        assert_eq!(Ok(L.to_owned()), build_url(&cfg));
        cfg.os(OS::Windows);
        cfg.archive(Archive::EXE);
        assert_eq!(Ok(M.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(N.to_owned()), build_url(&cfg));
    }

    #[test]
    fn jdk7_build_url() {
        let mut cfg: DownloadConfig = Default::default();

        // 7u80 JDKs
        cfg.version(Version::Seven);
        cfg.point_release(80);
        assert_eq!(Ok(O.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(P.to_owned()), build_url(&cfg));
        cfg.arch(Arch::X64);
        cfg.archive(Archive::RPM);
        assert_eq!(Ok(Q.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(R.to_owned()), build_url(&cfg));
        cfg.arch(Arch::X64);
        cfg.os(OS::Mac);
        cfg.archive(Archive::DMG);
        assert_eq!(Ok(S.to_owned()), build_url(&cfg));
        cfg.os(OS::Windows);
        cfg.archive(Archive::EXE);
        assert_eq!(Ok(T.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(U.to_owned()), build_url(&cfg));
    }

    #[test]
    fn jre7_build_url() {
        let mut cfg: DownloadConfig = Default::default();

        // 7u80 JREs
        cfg.version(Version::Seven);
        cfg.point_release(80);
        cfg.package(Package::JRE);
        assert_eq!(Ok(V.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(W.to_owned()), build_url(&cfg));
        cfg.arch(Arch::X64);
        cfg.archive(Archive::RPM);
        assert_eq!(Ok(X.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(Y.to_owned()), build_url(&cfg));
        cfg.arch(Arch::X64);
        cfg.os(OS::Mac);
        cfg.archive(Archive::DMG);
        assert_eq!(Ok(Z.to_owned()), build_url(&cfg));
        cfg.os(OS::Windows);
        cfg.archive(Archive::EXE);
        assert_eq!(Ok(AA.to_owned()), build_url(&cfg));
        cfg.arch(Arch::I586);
        assert_eq!(Ok(AB.to_owned()), build_url(&cfg));
    }

    #[test]
    fn bad_archive() {
        let mut cfg: DownloadConfig = Default::default();
        // exe archive not valid for linux.
        cfg.archive(Archive::EXE);
        match cfg.check_config() {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }

    #[test]
    fn bad_archive_1() {
        let mut cfg: DownloadConfig = Default::default();
        // dmg archive not valid for linux.
        cfg.archive(Archive::DMG);
        match cfg.check_config() {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }

    #[test]
    fn bad_archive_2() {
        let mut cfg: DownloadConfig = Default::default();
        cfg.os(OS::Mac);
        // tar.gz archive not valid for macosx.
        cfg.archive(Archive::TGZ);
        match cfg.check_config() {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }

    #[test]
    fn bad_archive_3() {
        let mut cfg: DownloadConfig = Default::default();
        cfg.os(OS::Mac);
        // rpm archive not valid for macosx.
        cfg.archive(Archive::RPM);
        match cfg.check_config() {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }

    #[test]
    fn bad_archive_4() {
        let mut cfg: DownloadConfig = Default::default();
        cfg.os(OS::Mac);
        // exe archive not valid for macosx.
        cfg.archive(Archive::EXE);
        match cfg.check_config() {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }

    #[test]
    fn bad_archive_5() {
        let mut cfg: DownloadConfig = Default::default();
        cfg.os(OS::Windows);
        // tar.gz archive not valid for windows.
        cfg.archive(Archive::TGZ);
        match cfg.check_config() {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }

    #[test]
    fn bad_archive_6() {
        let mut cfg: DownloadConfig = Default::default();
        cfg.os(OS::Windows);
        // rpm archive not valid for windows.
        cfg.archive(Archive::RPM);
        match cfg.check_config() {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }

    #[test]
    fn bad_archive_7() {
        let mut cfg: DownloadConfig = Default::default();
        cfg.os(OS::Windows);
        // dmb archive not valid for windows.
        cfg.archive(Archive::DMG);
        match cfg.check_config() {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }

    #[test]
    fn bad_arch_mac() {
        let mut cfg: DownloadConfig = Default::default();
        cfg.os(OS::Mac);
        // 32-bit not valid for macosx.
        cfg.arch(Arch::I586);
        match cfg.check_config() {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }

    #[test]
    fn dry_run() {
        let mut cfg: DownloadConfig = Default::default();
        cfg.dry_run(true);

        // dry run
        assert_eq!(Ok(0), cfg.download());
    }
}
