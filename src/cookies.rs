//! libwgetj API Cookie Handling
use std::env;
use std::fs::{self, File};
use std::io::{self, Write};
use std::path::PathBuf;

/// Version 8 JDK Cookie
pub static COOKIES_8_JDK: &'static [u8] = b"# Cookies for domains related to oracle.com.
# This content may be pasted into a cookies.txt file and used by wget
# Example:  wget -x --load-cookies cookies.txt \
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
#
www.oracle.com	FALSE	/technetwork/java/javase/downloads	FALSE	0	testSessionCookie	Enabled
www.oracle.com	FALSE	/	FALSE	0	JSESSIONID	\
PRRLTwhLvGX3T1V2Mb1GPKPy9c38Xn9mspn15fp52PTdDtvrrQpT!-593619270!1487296478
.oracle.com	TRUE	/	FALSE	1398258763.627247	Order_MarketingTrigger	WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1398258768.151734	Order_MarketingCampaignSuccess	\
WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1398258768.151784	p_mcc	\
NA:WWOU13045613MPP002C002:WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	0	p_org_id	1001
.oracle.com	TRUE	/	FALSE	0	p_lang	US
.oracle.com	TRUE	/	FALSE	0	p_cur_URL	\
http://education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=3
.oracle.com	TRUE	/	FALSE	1458821572	s_fid	1D59707C4A5F2EE8-36C9439BC0B23D90
.oracle.com	TRUE	/	FALSE	0	s_eVar21	WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1553429573	s_ev43	\
%5B%5B%27WWOU13045613MPP002C002%27%2C%271395663173003%27%5D%5D
education.oracle.com	FALSE	/	FALSE	0	fs_nocache_guid	51355CC33440384088C0D82E02C6738C
docs.oracle.com	FALSE	/	FALSE	0	tutorial_showLeftBar	yes
.edelivery.oracle.com	TRUE	/	FALSE	1713124283.261694	ARU_LANG	US
.oracle.com	TRUE	/	FALSE	1400689265	s_nr	1398097265323
.oracle.com	TRUE	/	FALSE	0	s_cc	true
.oracle.com	TRUE	/	FALSE	0	oraclelicense	accept-securebackup-cookie
.oracle.com	TRUE	/	FALSE	0	gpw_e24	\
http%3A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Fdownloads%2Fjdk8-downloads-2133151.html
.oracle.com	TRUE	/	FALSE	0	s_sq	\
oracleotnlive%2Coracleglobal%3D%2526pid%253Dotn%25253Aen-us%25253A%25252Fjava%25252Fjavase%25252F\
downloads%25252Fjdk8-downloads-2133151.html%2526pidt%253D1%2526oid%253D\
functiononclick(event)%25257BacceptAgreement(window.self%25252C'jdk-8u5-oth-JPR')\
%25253B%25257D%2526oidt%253D2%2526ot%253DRADIO
";

/// Version 8 JRE Cookie
pub static COOKIES_8_JRE: &'static [u8] = b"# Cookies for domains related to oracle.com.
# This content may be pasted into a cookies.txt file and used by wget
# Example:  wget -x --load-cookies cookies.txt \
http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
#
www.oracle.com	FALSE	/technetwork/java/javase/downloads	FALSE	0	testSessionCookie	Enabled
www.oracle.com	FALSE	/	FALSE	0	JSESSIONID	\
PRRLTwhLvGX3T1V2Mb1GPKPy9c38Xn9mspn15fp52PTdDtvrrQpT!-593619270!1487296478
.oracle.com	TRUE	/	FALSE	1398258763.627247	Order_MarketingTrigger	WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1398258768.151734	Order_MarketingCampaignSuccess	\
WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1398258768.151784	p_mcc	\
NA:WWOU13045613MPP002C002:WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	0	p_org_id	1001
.oracle.com	TRUE	/	FALSE	0	p_lang	US
.oracle.com	TRUE	/	FALSE	0	p_cur_URL	\
http://education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=3
.oracle.com	TRUE	/	FALSE	1458821572	s_fid	1D59707C4A5F2EE8-36C9439BC0B23D90
.oracle.com	TRUE	/	FALSE	0	s_eVar21	WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1553429573	s_ev43	\
%5B%5B%27WWOU13045613MPP002C002%27%2C%271395663173003%27%5D%5D
education.oracle.com	FALSE	/	FALSE	0	fs_nocache_guid	51355CC33440384088C0D82E02C6738C
docs.oracle.com	FALSE	/	FALSE	0	tutorial_showLeftBar	yes
.edelivery.oracle.com	TRUE	/	FALSE	1713124283.261694	ARU_LANG	US
.oracle.com	TRUE	/	FALSE	1400689265	s_nr	1398097265323
.oracle.com	TRUE	/	FALSE	0	s_cc	true
.oracle.com	TRUE	/	FALSE	0	oraclelicense	accept-securebackup-cookie
.oracle.com	TRUE	/	FALSE	0	gpw_e24	\
http%3A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Fdownloads%2Fjre8-downloads-2133155.html
.oracle.com	TRUE	/	FALSE	0	s_sq	\
oracleotnlive%2Coracleglobal%3D%2526pid%253Dotn%25253Aen-us%25253A%25252Fjava%25252Fjavase%25252F\
downloads%25252Fjre8-downloads-2133155.html%2526pidt%253D1%2526oid%253D\
functiononclick(event)%25257BacceptAgreement(window.self%25252C'jre-8u5-oth-JPR')\
%25253B%25257D%2526oidt%253D2%2526ot%253DRADIO";

/// Version 7 JDK Cookie
pub static COOKIES_7_JDK: &'static [u8] = b"# Cookies for domains related to oracle.com.
# This content may be pasted into a cookies.txt file and used by wget
# Example:  wget -x --load-cookies cookies.txt \
http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html
#
www.oracle.com	FALSE	/technetwork/java/javase/downloads	FALSE	0	testSessionCookie	Enabled
www.oracle.com	FALSE	/	FALSE	0	JSESSIONID	\
PRRLTwhLvGX3T1V2Mb1GPKPy9c38Xn9mspn15fp52PTdDtvrrQpT!-593619270!1487296478
.oracle.com	TRUE	/	FALSE	1398258763.627247	Order_MarketingTrigger	WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1398258768.151734	Order_MarketingCampaignSuccess	\
WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1398258768.151784	p_mcc	\
NA:WWOU13045613MPP002C002:WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	0	p_org_id	1001
.oracle.com	TRUE	/	FALSE	0	p_lang	US
.oracle.com	TRUE	/	FALSE	0	p_cur_URL	\
http://education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=3
.oracle.com	TRUE	/	FALSE	1458821572	s_fid	1D59707C4A5F2EE8-36C9439BC0B23D90
.oracle.com	TRUE	/	FALSE	0	s_eVar21	WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1553429573	s_ev43	\
%5B%5B%27WWOU13045613MPP002C002%27%2C%271395663173003%27%5D%5D
education.oracle.com	FALSE	/	FALSE	0	fs_nocache_guid	51355CC33440384088C0D82E02C6738C
docs.oracle.com	FALSE	/	FALSE	0	tutorial_showLeftBar	yes
.edelivery.oracle.com	TRUE	/	FALSE	1713124283.261694	ARU_LANG	US
.oracle.com	TRUE	/	FALSE	1400689265	s_nr	1398097265323
.oracle.com	TRUE	/	FALSE	0	s_cc	true
.oracle.com	TRUE	/	FALSE	0	oraclelicense	accept-securebackup-cookie
.oracle.com	TRUE	/	FALSE	0	gpw_e24	\
http%3A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Fdownloads%2Fjdk7-downloads-1880260.html
.oracle.com	TRUE	/	FALSE	0	s_sq	\
oracleotnlive%2Coracleglobal%3D%2526pid%253Dotn%25253Aen-us%25253A%25252Fjava%25252Fjavase%25252F\
downloads%25252Fjdk7-downloads-1880260.html%2526pidt%253D1%2526oid%253D\
functiononclick(event)%25257BacceptAgreement(window.self%25252C'jdk-7u55-oth-JPR')\
%25253B%25257D%2526oidt%253D2%2526ot%253DRADIO";

/// Version 7 JRE Cookie
pub static COOKIES_7_JRE: &'static [u8] = b"# Cookies for domains related to oracle.com.
# This content may be pasted into a cookies.txt file and used by wget
# Example:  wget -x --load-cookies cookies.txt \
http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html
#
www.oracle.com	FALSE	/technetwork/java/javase/downloads	FALSE	0	testSessionCookie	Enabled
www.oracle.com	FALSE	/	FALSE	0	JSESSIONID	\
PRRLTwhLvGX3T1V2Mb1GPKPy9c38Xn9mspn15fp52PTdDtvrrQpT!-593619270!1487296478
.oracle.com	TRUE	/	FALSE	1398258763.627247	Order_MarketingTrigger	WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1398258768.151734	Order_MarketingCampaignSuccess	\
WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1398258768.151784	p_mcc	\
NA:WWOU13045613MPP002C002:WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	0	p_org_id	1001
.oracle.com	TRUE	/	FALSE	0	p_lang	US
.oracle.com	TRUE	/	FALSE	0	p_cur_URL	\
http://education.oracle.com/pls/web_prod-plq-dad/db_pages.getpage?page_id=3
.oracle.com	TRUE	/	FALSE	1458821572	s_fid	1D59707C4A5F2EE8-36C9439BC0B23D90
.oracle.com	TRUE	/	FALSE	0	s_eVar21	WWOU13045613MPP002C002
.oracle.com	TRUE	/	FALSE	1553429573	s_ev43	\
%5B%5B%27WWOU13045613MPP002C002%27%2C%271395663173003%27%5D%5D
education.oracle.com	FALSE	/	FALSE	0	fs_nocache_guid	51355CC33440384088C0D82E02C6738C
docs.oracle.com	FALSE	/	FALSE	0	tutorial_showLeftBar	yes
.edelivery.oracle.com	TRUE	/	FALSE	1713124283.261694	ARU_LANG	US
.oracle.com	TRUE	/	FALSE	1400689265	s_nr	1398097265323
.oracle.com	TRUE	/	FALSE	0	s_cc	true
.oracle.com	TRUE	/	FALSE	0	oraclelicense	accept-securebackup-cookie
.oracle.com	TRUE	/	FALSE	0	gpw_e24	\
http%3A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Fdownloads%2Fjre7-downloads-1880261.html
.oracle.com	TRUE	/	FALSE	0	s_sq	\
oracleotnlive%2Coracleglobal%3D%2526pid%253Dotn%25253Aen-us%25253A%25252Fjava%25252Fjavase%25252F\
downloads%25252Fjre7-downloads-1880261.html%2526pidt%253D1%2526oid%253D\
functiononclick(event)%25257BacceptAgreement(window.self%25252C'jre-7u55-oth-JPR')\
%25253B%25257D%2526oidt%253D2%2526ot%253DRADIO";

/// Delete a temporary cookie file.
fn delete_tmp_file(filename: &str) -> io::Result<()> {
    let tmpfile = env::temp_dir().join(filename);

    if tmpfile.exists() {
        try!(fs::remove_file(&tmpfile));
    }

    Ok(())
}

/// Delete the temporary cookie files.
pub fn delete_tmp_files() -> Result<i32, ::LibwgetjError> {
    let paths = vec!["cookies_7_jdk",
                     "cookies_7_jre",
                     "cookies_8_jdk",
                     "cookies_8_jre"];

    for path in &paths {
        try!(delete_tmp_file(path));
    }

    Ok(0)
}

/// Write a temporary cookie file.
fn write_file(filename: &str, cookies: &[u8]) -> io::Result<PathBuf> {
    let tmpfile = env::temp_dir().join(filename);
    let mut cookie_file = try!(File::create(tmpfile.as_path()));
    try!(cookie_file.write_all(cookies));
    Ok(tmpfile)
}

/// Write the temporary cookie files.
pub fn write_tmp_files(cfg: &::DownloadConfig) -> Result<PathBuf, ::LibwgetjError> {
    match (cfg.ver, cfg.pkg) {
        (::Seven, ::JDK) => Ok(try!(write_file("cookies_7_jdk", COOKIES_7_JDK))),
        (::Seven, ::JRE) => Ok(try!(write_file("cookies_7_jre", COOKIES_7_JRE))),
        (::Eight, ::JDK) => Ok(try!(write_file("cookies_8_jdk", COOKIES_8_JDK))),
        (::Eight, ::JRE) => Ok(try!(write_file("cookies_8_jre", COOKIES_8_JRE))),
    }
}
