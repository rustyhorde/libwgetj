//! Types used to describe a Java download.
use self::Arch::{I586, X64};
use self::Archive::{RPM, TGZ, DMG, EXE};
use self::OS::{Linux, Mac, Windows};
use self::Package::{JRE, JDK};
use self::Version::{Seven, Eight};
use std::collections::HashMap;
use std::fmt;

/// CPU Architecture
#[derive(Clone, Copy)]
pub enum Arch {
    /// 32-bit
    I586,
    /// 64-bit
    X64,
}

impl fmt::Display for Arch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            I586 => write!(f, "i586"),
            X64 => write!(f, "x64"),
        }
    }
}

#[derive(Clone, Copy)]
/// The download archive type.
pub enum Archive {
    /// .rpm
    RPM,
    /// .tar.gz
    TGZ,
    /// .dmg
    DMG,
    /// .exe
    EXE,
}

impl fmt::Display for Archive {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            RPM => write!(f, "rpm"),
            TGZ => write!(f, "tar.gz"),
            DMG => write!(f, "dmg"),
            EXE => write!(f, "exe"),
        }
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
/// Supported OS downloads
pub enum OS {
    /// Linux
    Linux,
    /// Mac OSX
    Mac,
    /// Windows
    Windows,
}

impl fmt::Display for OS {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Linux => write!(f, "linux"),
            Mac => write!(f, "macosx"),
            Windows => write!(f, "windows"),
        }
    }
}

#[derive(Clone, Copy)]
/// Java package type.
pub enum Package {
    /// Java Runtime Environment
    JRE,
    /// Java Development Kit
    JDK,
}

impl fmt::Display for Package {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            JRE => write!(f, "jre"),
            JDK => write!(f, "jdk"),
        }
    }
}

/// Java Major Version
#[derive(Clone, Copy, PartialEq)]
pub enum Version {
    /// Java 7
    Seven,
    /// Java 8
    Eight,
}

macro_rules! hashmap {
    ($( $key: expr => $val: expr ),*) => {{
         let mut map = ::std::collections::HashMap::new();
         $( map.insert($key, $val); )*
         map
    }}
}

impl Version {
    /// Grab the point release to url component HashMap for a given major version.
    ///
    /// # Examples
    ///
    /// ```
    /// use libwgetj::Version::*;
    ///
    /// let pr = Seven.point_releases();
    /// assert!(!pr.is_empty());
    /// ```
    pub fn point_releases(self) -> HashMap<u8, u8> {
        match self {
            Seven => {
                hashmap![0 => 0, 1 => 8, 2 => 13, 3 => 4, 4 => 20, 5 => 6, 6 => 24, 7 => 10,
                              9 => 5, 10 => 18, 11 => 21, 13 => 20, 15 => 3, 17 => 2, 21 => 11,
                              25 => 15, 40 => 43, 45 => 18, 51 => 13, 55 => 13, 60 => 19, 65 => 17,
                              67 => 1, 71 => 14, 72 => 14, 75 => 13, 76 => 13, 79 => 15, 80 => 15]
            }
            Eight => {
                hashmap![0 => 132, 5 => 13, 11 => 12, 20 => 26, 25 => 17, 31 => 13, 40 => 26,
                              45 => 14, 51 => 16, 60 => 27, 65 => 17, 66 => 17, 71 => 15, 73 => 2,
                              74 => 2, 77 => 3, 91 => 14, 92 => 14, 101 => 13, 102 => 14,
                              112 => 15, 121 => 13, 131 => 11]
            }
        }
    }
}

impl fmt::Display for Version {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Seven => write!(f, "7"),
            Eight => write!(f, "8"),
        }
    }
}
