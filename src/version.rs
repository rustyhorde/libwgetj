//! Version Output
include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[cfg(unix)]
/// Generate a long version string.
fn verbose_ver() -> String {
    format!("\x1b[32;1mlibwgetj {}\x1b[0m ({} {}) (built {})\ncommit-hash: {}\ncommit-date: \
             {}\nbuild-date: {}\nhost: {}\nrelease: {}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver())
}

#[cfg(windows)]
/// Generate a long version string.
fn verbose_ver() -> String {
    format!("libwgetj {} ({} {}) (built {})\ncommit-hash: {}\ncommit-date: {}\nbuild-date: \
             {}\nhost: {}\nrelease: {}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver())
}

#[cfg(unix)]
/// Generate a short version string.
fn ver() -> String {
    format!("\x1b[32;1mlibwgetj {}\x1b[0m ({} {}) (built {})",
            semver(),
            short_sha(),
            commit_date(),
            short_now())
}

#[cfg(windows)]
/// Generate a short version string.
fn ver() -> String {
    format!("libwgetj {}[0m ({} {}) (built {})",
            semver(),
            short_sha(),
            commit_date(),
            short_now())
}

/// Generate a version string.
///
/// # Examples
/// ```
/// use libwgetj;
///
/// // Normal
/// println!("{}", libwgetj::version(false));
/// // rl-sys v0.1.3-pre-11-gd90443d (d90443d 2015-12-07) (built 2015-12-07)
///
/// // Verbose
/// println!("{}", libwgetj::version(true));
/// // rl-sys v0.1.3-pre-11-gd90443d (d90443d 2015-12-07) (built 2015-12-07)
/// // commit-hash: d90443d92db3826c648817e6bd6cb757729f7209
/// // commit-date: 2015-12-07
/// // build-date: 2015-12-07
/// // host: x86_64-unknown-linux-gnu
/// // release: v0.1.3-pre-11-gd90443d
/// ```
pub fn version(verbose: bool) -> String {
    if verbose { verbose_ver() } else { ver() }
}
